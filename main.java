class Main {
  public static void main(String[] args) {
    int n = 10;
    double[] x = {10, 9.8, 8, 7.8, 7.7, 7, 6, 5, 4, 2 };
    double[] y = {200, 44, 32, 24, 22, 17, 15, 12, 8, 4};
    System.out.println(avg(x,10));
    System.out.println(stdev(x,10));
    System.out.println(avg(y,10));
    System.out.println(stdev(y,10));
    System.out.println(corr(x,y,n));
    
  }

  public static double avg(double[] list, int n){
    double sum = 0;
    for (int i=0; i<n; i++){
      sum += list[i];
    }
    return  sum/n;
  }

  public static double stdev(double[] list, int n){
    double sum = 0;
    double mean = avg(list, n);
    for (int i=0; i<n; i++){
      sum += Math.pow(list[i]-mean, 2);
    }
    return  Math.sqrt(sum/n);
  }

public static double corr(double[] x, double[] y, int n){
    double sum = 0;
    double xMean = avg(x, n);
    double yMean = avg(y, n);
    double xDev = stdev(x, n);
    double yDev = stdev(y, n);
    for (int i=0; i<n; i++){
      sum += (x[i]-xMean)*(y[i]-yMean);
    }
    return  sum/(n*xDev*yDev);
  }  
}
