import math

def avg(list, n):
  sum = 0
  for i in range(n):
    sum += list[i]    
  return  sum/n
  

def stdev(list, n):
  sum = 0
  mean = avg(list, n)
  for i in range(n):
    sum += math.pow(list[i]-mean, 2)
  return  math.sqrt(sum/n)

def corr(x, y, n):
    sum = 0
    xMean = avg(x, n)
    yMean = avg(y, n)
    xDev = stdev(x, n)
    yDev = stdev(y, n)
    for i in range(n):
      sum += (x[i]-xMean)*(y[i]-yMean)
    return  sum/(n*xDev*yDev); 

n = int(input().strip())
x_list = input().strip()
listX = x_list.split(" ")
x = list(map(float, listX))
y_list = input().strip()
listY = y_list.split(" ")
y = list(map(float, listY))
print(corr(x,y,n))
